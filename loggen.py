from optparse import OptionParser
import logging
import os
from time import sleep
import random

DEFAULT_SEED = os.urandom(24)

LOGFORMAT = '%(levelname)s: %(asctime)s %(message)s'
logging.basicConfig(format=LOGFORMAT, level=logging.DEBUG, datefmt='%d-%m-%Y %H:%M:%S')
logger = logging.getLogger('logserver')


def generate_messages(max_log_entries):
    """
    Generate a bunch of random messages for the duration the application runs with a division
    set by random
    :return: nothing
    """
    counter = 0
    logger.debug('Waiting for %d messages' % max_log_entries)
    while counter < max_log_entries:
        sleep(random.randint(1, 10))
        threshold = random.randint(0, 100)
        if threshold < 80:
            logger.info('Random message generated')
        elif threshold < 95:
            logger.warning('Random Warning generated')
        else:
            logger.error('Error message generated')
        counter += 1


def parse_commandline():
    """
    Read the command line parameters.

    :return: command line option dictionary and argument list
    """
    usage = "%prog [options] [logfile1 logfile2 ...]\n\n" \
            "Generate a random log file to all files named as arguments. Defaults also " \
            "to standard output"
    parser = OptionParser(usage=usage, version="%prog 1.0")
    parser.add_option('-s', '--seed', action="store", dest="seed",
                      default=DEFAULT_SEED, help='seed to be used by randomizer. When not set a random value is used')
    parser.add_option('-m', '--max-events', action="store", dest="max_events",
                      default=12, help="maximum number of events generated (averages to 1 minute of generating events)")

    (options, args) = parser.parse_args()
    logger.warning('Commandline parser invocated')

    return options, args


def main():
    """
    This is a small script that generates more or less random log messages
    to test the behavior of ELK or Splunk type of monitors.
    Logging is added to all files named on the command line
    """
    (options, args) = parse_commandline()

    if len(args) < 1:
        logger.warning('Logging only to stdout')
    else:
        for logfile in args:
            hdlr = logging.FileHandler(logfile)
            hdlr.setFormatter(logging.Formatter(LOGFORMAT))
            logger.addHandler(hdlr)

            logger.info('Added logging to %s' % logfile)
    generate_messages(int(options.max_events))


if __name__ == "__main__":
    main()
