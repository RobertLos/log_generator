# LOG Generator

This README documents the log file generator.

### What is this repository for? ###

* Generation of log files to test tooling like LogStash, Splunk or the like

### How do I get set up? ###

* pip install -r requirements
* run the python script
```
python loggen.py -h
```
A help text is displayed

## Status ##
2016-11-23 - First install of BitBucket repository. Not much code in it yet. Please wait a while before using it.

$$ \LaTeX $$
